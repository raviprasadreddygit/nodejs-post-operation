


var userCtrls = require('./user.Ctrl')

module.exports = function(app){

 

    app.get('/home', userCtrls.home);
    

    app.get('/about', userCtrls.about);

    app.post('/service', userCtrls.services);
    
    app.get('/products/:id/:name', userCtrls.products);


}
