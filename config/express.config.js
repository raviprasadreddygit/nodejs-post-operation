var express = require('express');

var bodyParser = require('body-parser');


var routes = require('../users/user.routes');


    

module.exports = function(){
    
    
    var app = express();

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    routes(app);

   
    return app;

}
